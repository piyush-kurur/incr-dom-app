open Core
open Bonsai
open Bonsai_web

let buttons bts =
  let open Vdom in
  Node.div ~attr:(Attr.(many [ class_ "buttons" ])) bts
;;

let button label ac =
  let open Vdom in
  Node.button ~attr:(Attr.many [
                         Attr.class_ "button";
                         Attr.class_ "is-primary";
                         Attr.on_click ac
    ]) [Node.text label]
;;

let panel head body =
  let open Vdom in
  let phead = Node.p ~attr:(Attr.many[Attr.class_ "panel-heading"])
                [Node.text head] in
  let pbody = Node.div ~attr:(Attr.many[Attr.class_ "panel-block"])
                body in

  Node.div ~attr:(Attr.many [Attr.class_ "panel"])
    [ phead ; pbody]

;;

module Counter = struct

  module Action = struct
    type t = Reset
           | Incr
           | Decr [@@deriving sexp_of]
  end

  let apply_action ~inject:_ ~schedule_event:_ c (a:Action.t) =
    match a with
    | Reset -> 0
    | Incr  -> c + 1
    | Decr  -> c - 1
  ;;
  let counter = state_machine0
                  [%here]
                  (module Int)
                  (module Action)
                  ~default_model:0
                  ~apply_action:apply_action
  ;;

  let view =
    let open Let_syntax in
    let%sub counter        = counter   in
    let%arr count, inject  = counter   in
    let incr = fun _ev -> inject Incr  in
    let decr = fun _ev -> inject Decr  in
    let rst  = fun _ev -> inject Reset in
    panel
      [%string "Value = %{count#Int}"]
      [ buttons
          [ button "-1" decr
          ; button ":=0" rst
          ; button "+1" incr
          ]
      ]
  ;;
end
