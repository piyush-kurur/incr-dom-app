{

  description = "A sample incr-dom based application";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
          opkgs = pkgs.ocamlPackages;
      in
        {
          devShell = pkgs.mkShell {
            buildInputs = [
              opkgs.ocaml
              opkgs.dune_3
              opkgs.findlib
              opkgs.utop
              opkgs.merlin
              # Ocaml libraries used by this
              opkgs.core
              opkgs.bonsai
              opkgs.incr_dom
              opkgs.ppx_jane
              opkgs.js_of_ocaml
              opkgs.js_of_ocaml-ppx
            ];
          };
        }
    );
}
